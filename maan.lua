local SINGLE_CHAR_TOKENS = {
    ["("] = "open-paren",
    [")"] = "close-paren",
    ["["] = "open-bracket",
    ["]"] = "close-bracket",
    ["{"] = "open-brace",
    ["}"] = "close-brace",

    [":"] = "colon",
    ["\'"] = "quote",
    ["`"] = "unquote",
}

local function new_token(lexeme, _type)
    return setmetatable({lexeme = lexeme, _type = _type}, {
        __tostring = function(self)
            return string.format("(%s '%s')", self._type, self.lexeme)
        end
    })
end

local function is_ws(c)
    return c == ' ' or
           c == '\n' or
           c == '\t' or
           c == '\r'
end

local function is_digit(c)
    return string.match(c, "%d+")
end

-- string -> list[token]
local function tokenize(code)
    local it = 1
    local last = #code

    local result = {}

    local function ch() return code:sub(it, it) end

    local function skip_ws()
        while it <= last and is_ws(ch()) do
            it = it + 1
        end
    end

    local function get_token()
        skip_ws()

        local c = ch()

        -- handle numbers
        do
            local is_neg = false
            local is_dec = false
            local start = it

            if ch() == '-' then
                is_neg = true
                it = it + 1
            end

            if ch() == '.' then
                is_dec = true
                it = it + 1
            end

            if is_digit(ch()) then
                if not is_neg and not is_dec then
                    start = it
                end

                while it <= last do
                    local c = ch()
                    if not is_digit(c) then
                        if c == '.' then
                            if is_dec then
                                break
                            else
                                is_dec = true
                            end
                        else
                            break
                        end
                    end
                    it = it + 1
                end
                return new_token(code:sub(start, it - 1), "number")
            end
        end

        -- handle single characters
        if SINGLE_CHAR_TOKENS[c] then
            it = it + 1
            return new_token(c, SINGLE_CHAR_TOKENS[c])
        end

        -- handle atoms
        local start = it
        while it <= last and not is_ws(ch()) do
            it = it + 1
        end

        if it == start then
            return new_token("none", "none")
        end

        return new_token(code:sub(start, it - 1), "atom")
    end

    while it <= last do
        table.insert(result, get_token())
    end

    return result
end

local function tokens_to_ast()

end

local function eval(ast, env)
end

local toks = tokenize(" 3.1415926 -32 -.23 123 .2 () hello32world ")

for i, t in ipairs(toks) do
    print(t)
end
