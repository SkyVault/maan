--[[

value = <number> | <atoms>

expr-list =  <expr>*
func-call = "(" <expr> <expr-list> ")"

expr = <value>
     | <func-call>
     | <lambda>

]]

(function factorial(n)
    (if (<= n 1)
        1
        (* n (factorial (- n 1)))))

(factorial 5)

function factorial(n)
    if n <= 1 then
        return 1
    else
        return n * factorial(n - 1)
    end
end

factorial(5)
